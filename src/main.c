/*
    Copyright (C) 2020-2022 Andreas Shimokawa

    This file is part of NIMI Adventure

    NIMI Adventure is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NIMI Adventure is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <genesis.h>
#include <resources.h>

//#define DEBUG
#define VERSION "V1.10-WIP"
#define FPS 60

#define NR_PLAYERS 2
#define NR_ITEMS 11

#define LEFT_EDGE 0
#define RIGHT_EDGE 320
#define TOP_EDGE 0
#define BOTTOM_EDGE 224

#define PLAYER_WIDTH 24
#define PLAYER_HEIGHT 32

#define PLAYERSTATE_STANDING 0
#define PLAYERSTATE_JUMPING 1
#define PLAYERSTATE_JUMPING_MOVING 2
#define PLAYERSTATE_MOVING 3
#define PLAYERSTATE_BACK 4

#define ITEM_AME 0
#define ITEM_COOKIE 1
#define ITEM_GUMMI 2
#define ITEM_AISU 3
#define ITEM_DAME 4
#define ITEM_SPEEDUP 5
#define ITEM_HEART 6

#define ITEM_WIDTH 16
#define ITEM_HEIGHT 16

#define ROPE_WIDTH 80
#define ROPE_HEIGHT 24

#define SUSHI_WIDTH 64
#define SUSHI_HEIGHT 40

#define GAMESTATE_MENU 0
#define GAMESTATE_ENDED 1
#define GAMESTATE_CATCHER 2
#define GAMESTATE_ROPE 3
#define GAMESTATE_SUSHI 4

#ifndef DEBUG
#define CATCHER_SCORELIMIT 250
#define ROPE_LIVES 3
#define SUSHI_TIMELIMIT 30
#else
#define CATCHER_SCORELIMIT 1
#define ROPE_LIVES 1
#define SUSHI_TIMELIMIT 6
#endif

#define SFX_JUMP 64
#define SFX_COLLECT 65
#define SFX_FAIL 66
#define SFX_EAT_NINA 67
#define SFX_EAT_MIYA 68

static const u8 jumptab_rope[] = {2, 2, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 2};

static const u8 jumptab[] = {3,  7,  10, 13, 16, 19, 23, 26, 29, 32, 34, 37, 40, 42, 45, 47, 49, 51, 53, 55, 56, 58, 59, 60, 61, 62, 63, 63, 64, 64,
                             64, 64, 63, 63, 62, 61, 60, 59, 58, 56, 55, 53, 51, 49, 47, 45, 42, 40, 37, 34, 32, 29, 26, 23, 19, 16, 13, 10, 7,  3};

static const u8 ropetab[] = {0, 0, 0, 0, 0, 0, 0,  0,  1,  1,  1,  1,  1,  2,  2,  2,  2,  2,  3,  3,  3,  4,  4,  4,  5,  5,  5,  6,  6,  7,
                             7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 15, 15, 16, 16, 17, 17, 18, 19, 19, 20, 20, 21, 22, 22, 23};

typedef struct {
    s16 pos_x;
    s16 pos_y;
    s8 speed_x;
    u8 speed;
    u16 effect_countdown;
    s8 jumpstate;
    u8 state;
    u16 score;
    u8 points;
    u8 lives;
    Sprite *sprite;
    Sprite *hearts[ROPE_LIVES];
} Player;

typedef struct {
    s16 pos_x;
    s16 pos_y;
    s8 speed_y;
    u8 type;
    Sprite *sprite;
} Item;

typedef struct {
    u8 pos;
    u8 speed;
    bool on_floor;
    Sprite *sprite;
} Rope;

typedef struct {
    u8 type;
    u8 frame;
    Sprite *sprite;
} Sushi;

u8 gamestate;
u16 countdown;

u16 cpu_joystate_old;
u16 cpu_player_active = TRUE;

Player player[NR_PLAYERS];
Rope rope[NR_PLAYERS];
Sushi sushi[NR_PLAYERS];
Item item[NR_ITEMS];

Map *bgb;

s16 scroll;
u16 scroll_speed;
u16 scroll_target;
u8 scroll_targetstate;

void set_scrolltarget(u16 target, u16 speed, u8 state)
{
    scroll_speed = speed;
    scroll_target = target;
    scroll_targetstate = state;
}

void set_gamestate(u8 newstate)
{
    gamestate = newstate;
    VDP_clearText(0, 1, 40);
    VDP_clearText(0, 13, 40);
    VDP_clearText(0, 15, 40);
    VDP_clearText(0, 17, 40);
    VDP_clearText(0, 23, 40);
    VDP_clearText(0, 25, 40);

    player[0].score = 0;
    player[1].score = 0;

    if (gamestate == GAMESTATE_MENU) {
        VDP_drawText(VERSION, 30, 1);
        player[0].pos_x = 50 + ROPE_WIDTH / 2 - PLAYER_WIDTH / 2;
        player[1].pos_x = RIGHT_EDGE - ROPE_WIDTH - 50 + ROPE_WIDTH / 2 - PLAYER_WIDTH / 2;

        for (u16 i = 0; i < NR_PLAYERS; i++) {
            Player *p = &player[i];

            p->points = 0;
            p->speed = 0;
            p->speed_x = 0;
            p->pos_y = BOTTOM_EDGE - PLAYER_HEIGHT - 1;
            p->jumpstate = -1;
            p->state = PLAYERSTATE_STANDING;
        }
        VDP_drawText("(C) 2020-2021 NINA & ANDREAS SHIMOKAWA", 1, 23);
        VDP_drawText("PUSH START", 15, 25);
    }
    else {
        VDP_drawText("NINA", 1, 1);
        VDP_drawText("MIYA", 30, 1);
    }

    if (gamestate == GAMESTATE_ROPE) {
        player[0].pos_x = 50 + ROPE_WIDTH / 2 - PLAYER_WIDTH / 2;
        player[1].pos_x = RIGHT_EDGE - ROPE_WIDTH - 50 + ROPE_WIDTH / 2 - PLAYER_WIDTH / 2;

        for (u16 i = 0; i < NR_PLAYERS; i++) {
            Player *p = &player[i];
            Rope *r = &rope[i];

            p->speed = 0;
            p->speed_x = 0;
            p->pos_y = BOTTOM_EDGE - PLAYER_HEIGHT - 1;
            p->lives = ROPE_LIVES;
            r->speed = 2;
            r->pos = 0;
            r->on_floor = 0;
            SPR_setVisibility(r->sprite, VISIBLE);
            for (u16 j = 0; j < ROPE_LIVES; j++) {
                SPR_setVisibility(p->hearts[j], VISIBLE);
            }
        }
    }
    else {
        for (u16 i = 0; i < NR_PLAYERS; i++) {
            SPR_setVisibility(rope[i].sprite, HIDDEN);
            for (u16 j = 0; j < ROPE_LIVES; j++) {
                SPR_setVisibility(player[i].hearts[j], HIDDEN);
            }
        }
    }
    if (gamestate == GAMESTATE_CATCHER) {
        countdown = 60 * FPS;

        for (u16 i = 0; i < NR_PLAYERS; i++) {
            Player *p = &player[i];
            p->pos_y = BOTTOM_EDGE - PLAYER_HEIGHT - 1;
            p->pos_x = 0;
            p->jumpstate = -1;
            p->state = PLAYERSTATE_STANDING;
            p->effect_countdown = 0;
            p->speed = 1;
            p->speed_x = 0;
            p->lives = 0;

            if (i == 0) {
                p->pos_x = LEFT_EDGE;
            }
            else {
                p->pos_x = RIGHT_EDGE - PLAYER_WIDTH;
            }
        }

        for (u16 i = 0; i < NR_ITEMS; i++) {
            Item *itm = &item[i];
            itm->pos_x = 2 + i * 30;
            itm->pos_y = 0;
            itm->type = ITEM_AME;
            itm->speed_y = 1;
            SPR_setPosition(itm->sprite, itm->pos_x, itm->pos_y);
            SPR_setAnim(itm->sprite, itm->type);
            SPR_setVisibility(item[i].sprite, VISIBLE);
        }
    }
    else {
        for (u16 i = 0; i < NR_ITEMS; i++) {
            SPR_setVisibility(item[i].sprite, HIDDEN);
        }
    }
    if (gamestate == GAMESTATE_SUSHI) {
        countdown = SUSHI_TIMELIMIT * FPS;
        player[0].pos_x = 50 + SUSHI_WIDTH / 2 - PLAYER_WIDTH / 2;
        player[1].pos_x = RIGHT_EDGE - SUSHI_WIDTH - 50 + SUSHI_WIDTH / 2 - PLAYER_WIDTH / 2;

        for (u16 i = 0; i < NR_PLAYERS; i++) {
            Player *p = &player[i];
            p->speed = 0;
            p->speed_x = 0;
            p->pos_y = BOTTOM_EDGE - PLAYER_HEIGHT - 1;
            p->state = PLAYERSTATE_BACK;
            sushi[i].type = 0;
            sushi[i].frame = 0;
            SPR_setVisibility(sushi[i].sprite, VISIBLE);
        }
    }
    else {
        for (u16 i = 0; i < NR_PLAYERS; i++) {
            SPR_setVisibility(sushi[i].sprite, HIDDEN);
        }
    }
}

void spawn_item(u8 i)
{
    if (countdown == 0) {
        item[i].type = ITEM_SPEEDUP;
        countdown = 60 * FPS;
    }
    else {
        item[i].type = random() % 5;
    }
    SPR_setAnim(item[i].sprite, item[i].type);
}

void check_collect()
{
    for (u16 i = 0; i < NR_ITEMS; i++) {
        s16 x = item[i].pos_x;
        s16 y = item[i].pos_y;
        for (u16 p = 0; p < NR_PLAYERS; p++) {
            if (x < player[p].pos_x + PLAYER_WIDTH && x + ITEM_WIDTH > player[p].pos_x && y < player[p].pos_y + PLAYER_HEIGHT &&
                y + ITEM_HEIGHT > player[p].pos_y) {
                item[i].pos_y = -random() % 100;
                if (item[i].type == ITEM_DAME) {
                    if (player[p].score > 1) {
                        player[p].score -= 2;
                    }
                    SND_startPlayPCM_XGM(SFX_FAIL, 1, SOUND_PCM_CH2);
                }
                else if (item[i].type == ITEM_SPEEDUP) {
                    player[p].speed = 2;
                    player[p].effect_countdown = 600;
                    SND_startPlayPCM_XGM(SFX_COLLECT, 1, SOUND_PCM_CH2);
                }
                else {
                    player[p].score += item[i].type + 1;
                    SND_startPlayPCM_XGM(SFX_COLLECT, 1, SOUND_PCM_CH2);
                }
                spawn_item(i);
            }
        }
    }
}

void update_scores()
{
    char str_score[6];

    sprintf(str_score, "%4d", player[0].score);
    VDP_drawText(str_score, 6, 1);
    sprintf(str_score, "%4d", player[1].score);
    VDP_drawText(str_score, 35, 1);
}

void check_gameend()
{
    u8 winner = 0;
    if (gamestate == GAMESTATE_CATCHER) {
        if (player[0].score >= CATCHER_SCORELIMIT) {
            winner |= 1;
        }
        if (player[1].score >= CATCHER_SCORELIMIT) {
            winner |= 2;
        }
    }
    else if (gamestate == GAMESTATE_ROPE) {
        if (rope[0].on_floor && player[0].jumpstate == -1) {
            rope[0].speed = 2;

            SPR_setVisibility(player[0].hearts[player[0].lives - 1], HIDDEN);
            SND_startPlayPCM_XGM(SFX_FAIL, 1, SOUND_PCM_CH2);

            if (--player[0].lives == 0) {
                winner |= 2;
            }
        }
        if (rope[1].on_floor && player[1].jumpstate == -1) {
            rope[1].speed = 2;

            SPR_setVisibility(player[1].hearts[player[1].lives - 1], HIDDEN);
            SND_startPlayPCM_XGM(SFX_FAIL, 1, SOUND_PCM_CH2);

            if (--player[1].lives == 0) {
                winner |= 1;
            }
        }
        if (winner) {
            if (winner == 1) {
                player[1].jumpstate = 0;
                player[0].jumpstate = sizeof(jumptab) - 1;
            }
            else if (winner == 2) {
                player[0].jumpstate = 0;
                player[1].jumpstate = sizeof(jumptab) - 1;
            }
            else if (winner == (1 | 2)) {
                player[0].jumpstate = 0;
                player[1].jumpstate = 0;
            }
        }
    }
    else if (gamestate == GAMESTATE_SUSHI) {
        if (countdown-- == 0) {
            if (player[0].score >= player[1].score) {
                winner |= 1;
            }
            if (player[1].score >= player[0].score) {
                winner |= 2;
            }
        }
    }
    if (winner) {
        player[0].speed = 1;
        player[1].speed = 1;

        if (winner == 1) {
            VDP_drawText("NINA WINS!", 15, 13);
            player[0].points += 2;
        }
        if (winner == 2) {
            VDP_drawText("MIYA WINS!", 15, 13);
            player[1].points += 2;
        }
        else if (winner == (1 | 2)) {
            VDP_drawText("DRAW!!", 17, 13);
            player[0].points++;
            player[1].points++;
        }
        if (gamestate == GAMESTATE_SUSHI) {
            if (player[0].points > player[1].points) {
                VDP_drawText("FINAL RESULT: NINA WINS!", 8, 15);
            }
            else if (player[1].points > player[0].points) {
                VDP_drawText("FINAL RESULT: MIYA WINS!", 8, 15);
            }
            else {
                VDP_drawText("FINAL RESULT: DRAW!", 11, 15);
            }

            VDP_drawText("THANK YOU FOR PLAYING!!!", 8, 17);
        }
        else {
            VDP_drawText("PUSH START FOR NEXT STAGE", 8, 15);
        }
        gamestate = GAMESTATE_ENDED;
    }
}

void check_timers()
{
    if (countdown > 0) {
        countdown--;
    }
    if (gamestate == GAMESTATE_CATCHER) {
        for (u16 i = 0; i < NR_PLAYERS; i++) {
            if (player[i].effect_countdown) {
                if (--player[i].effect_countdown == 0) {
                    player[i].speed = 1;
                }
            }
        }
    }
}

void joyhandler(u16 joy, u16 changed, u16 state)
{
    if (joy == JOY_2 && changed & BUTTON_START && state & BUTTON_START) {
        cpu_player_active = FALSE; // disable CPU player if second player presses START
    }
    if ((state & BUTTON_START) && (scroll_target == scroll) && (gamestate == GAMESTATE_ENDED || gamestate == GAMESTATE_MENU)) {
        set_gamestate(GAMESTATE_ENDED);
        if (scroll_targetstate == GAMESTATE_MENU) {
            set_scrolltarget(320, 4, GAMESTATE_CATCHER);
        }
        else if (scroll_targetstate == GAMESTATE_CATCHER) {
            set_scrolltarget(640, 4, GAMESTATE_ROPE);
        }
        else if (scroll_targetstate == GAMESTATE_ROPE) {
            set_scrolltarget(320, 4, GAMESTATE_SUSHI);
        }
        else {
            set_scrolltarget(0, 4, GAMESTATE_MENU);
        }
    }
    if (gamestate == GAMESTATE_MENU) {
        return;
    }
    Player *p;
    Sushi *s;

    if (joy == JOY_1) {
        p = &player[0];
        s = &sushi[0];
    }
    else {
        p = &player[1];
        s = &sushi[1];
    }

    if (gamestate == GAMESTATE_SUSHI) {
        if ((state & BUTTON_A) && (changed & BUTTON_A)) {
            s->frame++;
            if (s->frame > 2) {
                s->frame = 0;
                s->type++;
                if (s->type > 2) {
                    s->type = 0;
                }
            }
            if ((p->score++ & 0x1) == 0) // only play every 2nd time
            {
                if (joy == JOY_1) {
                    SND_startPlayPCM_XGM(SFX_EAT_NINA, 1, SOUND_PCM_CH1);
                }
                else {
                    SND_startPlayPCM_XGM(SFX_EAT_MIYA, 1, SOUND_PCM_CH2);
                }
            }
        }

        return;
    }

    if ((state & BUTTON_A) && (changed & BUTTON_A)) {
        if (p->jumpstate == -1) {
            p->jumpstate = 0;
            SND_startPlayPCM_XGM(SFX_JUMP, 1, SOUND_PCM_CH1);

            if (p->state != PLAYERSTATE_MOVING) {
                p->state = PLAYERSTATE_JUMPING;
            }
        }
    }
    if (state & (BUTTON_RIGHT | BUTTON_LEFT)) {
        if (p->jumpstate == -1) {
            p->state = PLAYERSTATE_MOVING;
        }
        else {
            p->state = PLAYERSTATE_JUMPING_MOVING;
        }
        if (state & BUTTON_RIGHT) {
            SPR_setHFlip(p->sprite, FALSE);
            p->speed_x = p->speed;
        }
        else {
            SPR_setHFlip(p->sprite, TRUE);
            p->speed_x = -p->speed;
        }
    }
    else {
        if ((changed & BUTTON_RIGHT) | (changed & BUTTON_LEFT)) {
            p->speed_x = 0;
            if (p->state == PLAYERSTATE_JUMPING_MOVING) {
                p->state = PLAYERSTATE_JUMPING;
            }
            else if (p->state != PLAYERSTATE_JUMPING) {
                p->state = PLAYERSTATE_STANDING;
            }
        }
    }
}

void init_hw()
{
    JOY_init();
    JOY_setEventHandler(&joyhandler);
    SPR_init(0, 0, 0);
    player[0].sprite = SPR_addSprite(&imgnina, 0, 0, TILE_ATTR(PAL2, 0, FALSE, FALSE));
    player[1].sprite = SPR_addSprite(&imgmiya, 0, 0, TILE_ATTR(PAL2, 0, FALSE, FALSE));

    for (u16 i = 0; i < NR_PLAYERS; i++) {
        Player *p = &player[i];
        SPR_setDepth(p->sprite, 0x5000);
        for (u16 j = 0; j < ROPE_LIVES; j++) {
            p->hearts[j] = SPR_addSprite(&imgitem, 40 + i * 232 + j * ITEM_WIDTH, 1, TILE_ATTR(PAL3, 0, FALSE, FALSE));
            SPR_setAnim(p->hearts[j], ITEM_HEART);
        }
    }

    for (u16 i = 0; i < NR_ITEMS; i++) {
        item[i].sprite = SPR_addSprite(&imgitem, 0, 0, TILE_ATTR(PAL3, 0, FALSE, FALSE));
    }
    rope[0].sprite = SPR_addSprite(&imgrope, 50, BOTTOM_EDGE - ROPE_HEIGHT, TILE_ATTR(PAL2, 0, FALSE, FALSE));
    rope[1].sprite = SPR_addSprite(&imgrope, RIGHT_EDGE - ROPE_WIDTH - 50, BOTTOM_EDGE - ROPE_HEIGHT, TILE_ATTR(PAL2, 0, FALSE, FALSE));

    sushi[0].sprite = SPR_addSprite(&imgsushi, 50, BOTTOM_EDGE - SUSHI_HEIGHT, TILE_ATTR(PAL3, 0, FALSE, FALSE));
    sushi[1].sprite = SPR_addSprite(&imgsushi, RIGHT_EDGE - SUSHI_WIDTH - 50, BOTTOM_EDGE - SUSHI_HEIGHT, TILE_ATTR(PAL3, 0, FALSE, FALSE));

    VDP_setTextPlane(BG_A);
    VDP_setTextPriority(TRUE);

    VDP_setPalette(PAL1, bgb_pal.data);
    VDP_setPalette(PAL0, imgmiya.palette->data);
    VDP_setPalette(PAL2, imgmiya.palette->data);
    VDP_setPalette(PAL3, imgitem.palette->data);

    SND_setPCM_XGM(SFX_JUMP, sfxjump, sizeof(sfxjump));
    SND_setPCM_XGM(SFX_COLLECT, sfxitem, sizeof(sfxitem));
    SND_setPCM_XGM(SFX_FAIL, sfxdame, sizeof(sfxdame));
    SND_setPCM_XGM(SFX_EAT_NINA, sfxeatnina, sizeof(sfxeatnina));
    SND_setPCM_XGM(SFX_EAT_MIYA, sfxeatmiya, sizeof(sfxeatmiya));

    VDP_loadTileSet(&bgb_tileset, TILE_USERINDEX, DMA);
    bgb = MAP_create(&bgb_map, BG_B, TILE_ATTR_FULL(PAL1, TRUE, FALSE, FALSE, TILE_USERINDEX));
}

void update_rope()
{
    for (u16 i = 0; i < NR_PLAYERS; i++) {
        u16 rope_x;
        if (i == 0) {
            rope_x = 50;
        }
        else {
            rope_x = RIGHT_EDGE - ROPE_WIDTH - 50;
        }
        if (rope[i].pos < 60) // rope moving from bottom to middle
        {
            SPR_setPosition(rope[i].sprite, rope_x, BOTTOM_EDGE - ROPE_HEIGHT);
            SPR_setFrame(rope[i].sprite, ropetab[rope[i].pos]);
            SPR_setVFlip(rope[i].sprite, FALSE);
            SPR_setDepth(rope[i].sprite, SPR_MAX_DEPTH);
        }
        else if (rope[i].pos < 2 * 60) { // middle to top
            SPR_setPosition(rope[i].sprite, rope_x, BOTTOM_EDGE - ROPE_HEIGHT - ROPE_HEIGHT);
            SPR_setFrame(rope[i].sprite, ropetab[60 - 1 - (rope[i].pos - 60)]);
            SPR_setVFlip(rope[i].sprite, TRUE);
            SPR_setDepth(rope[i].sprite, SPR_MAX_DEPTH);
        }
        else if (rope[i].pos < 3 * 60) { // top to middle
            SPR_setPosition(rope[i].sprite, rope_x, BOTTOM_EDGE - ROPE_HEIGHT - ROPE_HEIGHT);

            SPR_setFrame(rope[i].sprite, ropetab[(rope[i].pos - 60 * 2)]);
            SPR_setVFlip(rope[i].sprite, TRUE);
            SPR_setDepth(rope[i].sprite, SPR_MIN_DEPTH);
        }
        else { // middle to bottom
            SPR_setPosition(rope[i].sprite, rope_x, BOTTOM_EDGE - ROPE_HEIGHT);

            SPR_setFrame(rope[i].sprite, ropetab[60 - 1 - (rope[i].pos - 60 * 3)]);
            SPR_setVFlip(rope[i].sprite, FALSE);
            SPR_setDepth(rope[i].sprite, SPR_MIN_DEPTH);
        }
        rope[i].pos += rope[i].speed;

        if (rope[i].pos >= 240) {
            rope[i].on_floor = TRUE;
            rope[i].pos = rope[i].pos - 240;
            if (rope[i].speed < 7) {

                rope[i].speed++;
            }
        }
        else {
            rope[i].on_floor = FALSE;
        }
    }
}

void update_sushi()
{
    for (u16 i = 0; i < NR_PLAYERS; i++) {
        SPR_setAnimAndFrame(sushi[i].sprite, sushi[i].type, sushi[i].frame);
    }
}

void cpu_player_act()
{
    switch (gamestate) {
    case GAMESTATE_CATCHER:
        for (u16 i = NR_ITEMS - 1; i > 0; i--) {
            Item *itm = &item[i];

            if (itm->type == ITEM_AISU || itm->type == ITEM_GUMMI) {
                if (itm->pos_y > 100) {
                    u16 cpu_joystate;
                    if (player[1].pos_x < itm->pos_x - (PLAYER_WIDTH - ITEM_WIDTH) / 2 - 1) {
                        cpu_joystate = BUTTON_RIGHT;
                    }
                    else if (player[1].pos_x > itm->pos_x - (PLAYER_WIDTH - ITEM_WIDTH) / 2 - 1) {
                        cpu_joystate = BUTTON_LEFT;
                    }
                    else {
                        cpu_joystate = 0;
                    }

                    u16 cpu_joychanged = cpu_joystate_old ^ cpu_joystate;
                    if (cpu_joychanged & (BUTTON_LEFT | BUTTON_RIGHT)) {
                        cpu_joystate |= BUTTON_A;
                        cpu_joychanged |= BUTTON_A;
                    }

                    joyhandler(JOY_2, cpu_joychanged, cpu_joystate);
                    cpu_joystate_old = cpu_joystate;
                    break;
                }
            }
        }
        break;
    }
}

int main(u16 hard)
{
    scroll = -1;
    scroll_speed = 1;
    scroll_target = 0;
    scroll_targetstate = GAMESTATE_MENU;

    init_hw();
    set_gamestate(GAMESTATE_MENU);

    while (TRUE) {
        Player *p;
        for (u16 i = 0; i < NR_PLAYERS; i++) {
            p = &player[i];

            p->pos_x += p->speed_x;
            if (p->pos_x < LEFT_EDGE) {
                p->pos_x = LEFT_EDGE;
            }
            else if (p->pos_x + PLAYER_WIDTH > RIGHT_EDGE) {
                p->pos_x = RIGHT_EDGE - PLAYER_WIDTH;
            }

            if (p->jumpstate > -1) {
                if (gamestate == GAMESTATE_ROPE) {
                    p->pos_y = BOTTOM_EDGE - PLAYER_HEIGHT - 1 - jumptab_rope[p->jumpstate++];
                }
                else {
                    p->pos_y = BOTTOM_EDGE - PLAYER_HEIGHT - 1 - jumptab[p->jumpstate++];
                }
                if (p->jumpstate >= sizeof(jumptab) || (gamestate == GAMESTATE_ROPE && p->jumpstate >= sizeof(jumptab_rope))) {
                    p->pos_y = BOTTOM_EDGE - PLAYER_HEIGHT - 1;
                    p->jumpstate = -1;
                    if (p->state == PLAYERSTATE_JUMPING_MOVING) {
                        p->state = PLAYERSTATE_MOVING;
                    }
                    else if (gamestate == GAMESTATE_SUSHI) {
                        p->state = PLAYERSTATE_BACK;
                    }
                    else {
                        p->state = PLAYERSTATE_STANDING;
                    }
                }
            }

            SPR_setPosition(p->sprite, p->pos_x, p->pos_y);
            SPR_setAnim(p->sprite, p->state);
        }
        if (gamestate == GAMESTATE_CATCHER) {
            check_collect();

            for (u16 i = 0; i < NR_ITEMS; i++) {
                item[i].pos_y++;
                if (item[i].pos_y > BOTTOM_EDGE - ITEM_HEIGHT) {
                    item[i].pos_y = -random() % 100;
                    spawn_item(i);
                }
                SPR_setPosition(item[i].sprite, item[i].pos_x, item[i].pos_y);
            }
            update_scores();
            check_timers();
            check_gameend();
        }
        if (gamestate == GAMESTATE_ROPE) {
            update_rope();
            check_gameend();
        }
        if (gamestate == GAMESTATE_SUSHI) {
            update_scores();
            update_sushi();
            check_timers();
            check_gameend();
        }

        if (scroll_speed != 0) {
            bool scroll_finished = FALSE;
            if (scroll <= scroll_target) {
                scroll += scroll_speed;
                if (scroll >= scroll_target) {
                    scroll_finished = TRUE;
                }
            }
            if (scroll >= scroll_target) {
                scroll -= scroll_speed;
                if (scroll <= scroll_target) {
                    scroll_finished = TRUE;
                }
            }
            if (scroll_finished) {
                scroll = scroll_target;
                scroll_speed = 0;
                set_gamestate(scroll_targetstate);
            }

            MAP_scrollTo(bgb, scroll, 0);
        }

        SPR_update();
        if (cpu_player_active) {
            cpu_player_act();
        }

        SYS_doVBlankProcess();
    }
    return 0;
}
