# NIMI Adventure

<a href="https://codeberg.org/ashimokawa/NIMI_Adventure">
    <img alt="Get it on Codeberg" src="https://get-it-on.codeberg.org/get-it-on-blue-on-white.png" height="60">
</a>

![Title Screenshot](screenshots/title.png)

NIMI Adventure is a home-brew SEGA Mega Drive game created during Christmas
holiday by Nina and Andreas Shimokawa, mainly out of curiosity without any prior
knowledge of the system.

It is a two player competitive game and consists of three mini games.

## Mini Game 1: Sweets Catcher

![Screenshot #1](screenshots/stage_1_sweets_catcher.png)

The player who first reaches 250 or more points wins.

Controls:
- LEFT: Move left
- RIGHT: Move right
- A: Jump

The following items exist:
- Candy = 1 point
- Cookie = 2 points
- Gummy bear = 3 points
- Ice cream = 4 points
- Bad cookie = -2 points

There is one special item that occurs every 60 seconds (Rainbow Drink), this
will boost the player's movements for 10 seconds.

## Mini Game 2: Rope jumping

![Screenshot #2](screenshots/stage_2_rope_jumping.png)

The player who fails to jump over the rope three times, loses.

Controls:
- A: Jump

There are 6 speed levels, the game starts at level 1 and will go to the next
lever after each full twirl. Once at speed level 6 the movement will stay at
a constant rate.

## Mini Game 3: Sushi speed eating

![Screenshot #3](screenshots/stage_3_sushi_eating.png)

The game runs for 30 seconds, players just have to mash the A button as
many times as possible. The player who managed to eat the most sushi, wins.

Controls:
- A: Eat sushi

## Technical notes

The game was created on Linux with [SGDK](https://github.com/Stephane-D/SGDK)

To build the SDK for Linux the [gendev](https://github.com/kubilus1/gendev) was
used

NIMI Adventure 1.00 was compiled with a self-compiled gendev with patches to
bring to up to SGDK 1.60

The current main branch needs gendev 0.7.1 (SGDK 1.62), which makes it much
easier to build on Debian x86_64, since there is a deb package already available
on the gendev release page.

## Recommended Emulators

We are playing the game on a Raspberry Pi 4 running RetroPie 4.7.1 (using
lr-picodrive)

On Linux we found BlastEm to run very well (BlastEm is included in Debian,
Ubuntu and other distributions)

## Acknowledgments

The following sounds were converted from SuperTux sounds:

- supertux_coin.wav
- supertux_hurt.wav
- supertux_jump.wav
