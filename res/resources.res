TILESET bgb_tileset "gfx/background.png" BEST ALL
MAP bgb_map "gfx/background.png" bgb_tileset NONE 0
PALETTE bgb_pal "gfx/background.png"

SPRITE imgnina    "sprites/nina.png"  3   4   BEST 10
SPRITE imgmiya    "sprites/miya.png"  3   4   BEST 10
SPRITE imgitem    "sprites/okashi.png"  2   2   BEST
SPRITE imgsushi   "sprites/sushi.png"  8   5   BEST 0 
SPRITE imgrope    "sprites/rope.png"  10   3   BEST 0

WAV sfxjump "sfx/supertux_jump.wav" XGM
WAV sfxitem "sfx/supertux_coin.wav" XGM
WAV sfxdame "sfx/supertux_hurt.wav" XGM
WAV sfxeatnina "sfx/nina_eat.wav" XGM
WAV sfxeatmiya "sfx/miya_eat.wav" XGM
